package lab.stellar.web;

import lab.stellar.entities.PlanetarySystem;
import lab.stellar.service.StellarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/rest")
public class StellarREST {

    @Autowired
    StellarService service;

    @GetMapping("/systems")
    public List<PlanetarySystem> getAllSystems(){
        return service.getSystems();
    }


}
