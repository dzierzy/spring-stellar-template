package lab.stellar.web;

import lab.stellar.entities.Planet;
import lab.stellar.entities.PlanetarySystem;
import lab.stellar.service.StellarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
public class StellarController {

    @Autowired
    StellarService service;

    @RequestMapping(value = "/systems", method = RequestMethod.GET)
    public String getAllSystems(Model model){

        List<PlanetarySystem> systems = service.getSystems();
        model.addAttribute("systems", systems);

        return "systems";
    }

    @RequestMapping(value = "/planets", method = RequestMethod.GET)
    public String getPlanetsBySystem(Model model, @RequestParam("systemId") int systemId){

        PlanetarySystem system = service.getSystemById(systemId);
        List<Planet> planets = service.getPlanets(system);
        model.addAttribute("planets", planets);

        return "planets";
    }




}
