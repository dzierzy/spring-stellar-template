package lab.stellar.dao.impl.jpa;

import lab.stellar.dao.PlanetDAO;
import lab.stellar.entities.Planet;
import lab.stellar.entities.PlanetarySystem;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;
import java.util.logging.Logger;



public class JPAPlanetDAO implements PlanetDAO {

    Logger logger = Logger.getLogger(JPAPlanetDAO.class.getName());

    @PersistenceContext(name = "stellarUnit")
    EntityManager em;



    @Override
    public List<Planet> getAllPlanets() {
        logger.info("querying for all planetary systems");
        return em.createQuery("select p from Planet p").getResultList();
    }

    @Override
    public List<Planet> getPlanetsBySystem(PlanetarySystem system) {
        logger.info("querying for all planetary systems");
        return em.createQuery("select p from Planet p where p.system=:s")
                .setParameter("s", system)
                .getResultList();
    }

    @Override
    public List<Planet> getPlanetsBySystemAndName(PlanetarySystem system, String like) {
        logger.info("querying for all planetary systems");
        return em.createQuery("select p from Planet p where p.system=:s and p.name like :like")
                .setParameter("s", system)
                .setParameter("like", "%" + like + "%")
                .getResultList();
    }

    @Override
    public Planet getPlanetById(int id) {
        return em.find(Planet.class, id);
    }

    @Override
    public Planet addPlanet(Planet p) {
         em.persist(p);
         return p;
    }
}
